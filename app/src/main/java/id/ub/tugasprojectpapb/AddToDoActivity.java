package id.ub.tugasprojectpapb;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class AddToDoActivity extends AppCompatActivity implements View.OnClickListener {

    EditText etTitle;
    EditText etDesc;
    EditText etDate;
    EditText etClock;
    Button btSubmit;
    DatabaseReference database;
    FirebaseUser user;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_to_do);

        etTitle = findViewById(R.id.judul);
        etDesc = findViewById(R.id.deskripsi);
        etDate = findViewById(R.id.tanggal);
        etClock = findViewById(R.id.waktu);
        btSubmit = findViewById(R.id.btSubmitAddToDo);
        btSubmit.setOnClickListener(this);

        database = FirebaseDatabase.getInstance().getReference();
        user = FirebaseAuth.getInstance().getCurrentUser();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == btSubmit.getId()) {
            HashMap<String, String> toDoMap = new HashMap<>();
            toDoMap.put("title", etTitle.getText().toString());
            toDoMap.put("desc", etDesc.getText().toString());
            toDoMap.put("date", etDate.getText().toString());
            toDoMap.put("clock", etClock.getText().toString());
            toDoMap.put("status", "Not Completed");
            database.child("Task").child(user.getUid()).push().setValue(toDoMap);
            Toast.makeText(this, "Task has been added", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}