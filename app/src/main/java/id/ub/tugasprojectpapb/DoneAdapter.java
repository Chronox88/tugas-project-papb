package id.ub.tugasprojectpapb;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class DoneAdapter extends RecyclerView.Adapter<DoneAdapter.DoneViewHolder> {
    LayoutInflater inflater;
    Context _context;
    ArrayList<ToDoItem> data;
    DatabaseReference database;
    FirebaseUser user;
    String key;

    public DoneAdapter(Context _context, ArrayList<ToDoItem> data) {
        this._context = _context;
        this.data = data;
        this.inflater = LayoutInflater.from(this._context);
    }

    @NonNull
    @Override
    public DoneViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_to_do, parent, false);
        return new DoneViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DoneViewHolder holder, int position) {
        ToDoItem item = data.get(position);
        holder.tvToDo.setText(item.title);
        holder.tvDate.setText(item.date);
        holder.tvClock.setText(item.clock);
        key = item.key;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database = FirebaseDatabase.getInstance().getReference("Task");
                user = FirebaseAuth.getInstance().getCurrentUser();
                Intent intent = new Intent(_context, DetailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("key",key);
                _context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class DoneViewHolder extends RecyclerView.ViewHolder {
        TextView tvToDo;
        TextView tvDate;
        TextView tvClock;

        public DoneViewHolder(@NonNull View itemView) {
            super(itemView);
            tvToDo = itemView.findViewById(R.id.tvToDo1);
            tvDate = itemView.findViewById(R.id.tvDate1);
            tvClock = itemView.findViewById(R.id.tvClock1);
        }
    }
}

