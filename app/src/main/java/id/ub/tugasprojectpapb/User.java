package id.ub.tugasprojectpapb;

public class User {
    public String name;
    public String photo;

    public User() {}
    public User(String name, String photo) {
        this.name = name;
        this.photo = photo;
    }
}
