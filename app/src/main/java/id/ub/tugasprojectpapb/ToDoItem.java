package id.ub.tugasprojectpapb;

public class ToDoItem {
    public String key;
    public String title;
    public String date;
    public String clock;
    public String desc;
    public String status;

    public ToDoItem() {}
    public ToDoItem(String key, String title, String date, String clock, String desc, String status) {
        this.key = key;
        this.title = title;
        this.date = date;
        this.clock = clock;
        this.desc = desc;
        this.status = status;
    }
}
