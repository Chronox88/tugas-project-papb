package id.ub.tugasprojectpapb;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

public class DetailActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tvTitle;
    TextView tvDesc;
    TextView tvDate;
    TextView tvClock;
    TextView tvStatus;
    Button btDone;
    DatabaseReference ref;
    FirebaseUser user;
    String key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_to_do);

        user = FirebaseAuth.getInstance().getCurrentUser();
        key = getIntent().getStringExtra("key");
        ref = FirebaseDatabase.getInstance().getReference("Task").child(user.getUid()).child(key);

        tvTitle = findViewById(R.id.detailIsiJudul);
        tvDesc = findViewById(R.id.detailIsiDeskripsi);
        tvDate = findViewById(R.id.detailIsiTanggal);
        tvClock = findViewById(R.id.detailIsiWaktu);
        tvStatus = findViewById(R.id.detailIsiStatus);
        btDone = findViewById(R.id.btDone);
        btDone.setOnClickListener(this);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                tvTitle.setText(snapshot.child("title").getValue().toString());
                tvDate.setText(snapshot.child("date").getValue().toString());
                tvClock.setText(snapshot.child("clock").getValue().toString());
                tvDesc.setText(snapshot.child("desc").getValue().toString());
                tvStatus.setText(snapshot.child("status").getValue().toString());

                if(snapshot.child("status").getValue().toString().equals("Completed")) {
                    btDone.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        ref.child("status").setValue("Completed");
        finish();
    }
}