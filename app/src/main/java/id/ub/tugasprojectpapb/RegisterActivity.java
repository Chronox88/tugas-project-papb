package id.ub.tugasprojectpapb;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    EditText etNama;
    EditText etEmail;
    EditText etPass;
    Button btRegister;
    Button btLogin;
    private FirebaseAuth mAuth;
    DatabaseReference ref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        etNama = findViewById(R.id.etNama);
        etEmail = findViewById(R.id.etEmailRegister);
        etPass = findViewById(R.id.etPassRegister);
        btRegister = findViewById(R.id.btRegisterRegister);
        btLogin =  findViewById(R.id.btLoginRegister);
        mAuth = FirebaseAuth.getInstance();
        ref = FirebaseDatabase.getInstance().getReference("User");
        btRegister.setOnClickListener(this);
        btLogin.setOnClickListener(this);
    }

    private void createUser() {
        String email = etEmail.getText().toString();
        String password = etPass.getText().toString();
        String nama = etNama.getText().toString();

        if (TextUtils.isEmpty(email)) {
            etEmail.setError("Email tidak bisa kosong!");
            etEmail.requestFocus();
        } else if(TextUtils.isEmpty(password)) {
            etPass.setError("Password tidak bisa kosong!");
            etPass.requestFocus();
        } else if (TextUtils.isEmpty(nama)) {
            etNama.setError("Nama tidak bisa kosong!");
            etNama.requestFocus();
        } else {
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                if(mAuth != null) {
                                    HashMap<String, String> userMap = new HashMap<>();
                                    userMap.put("name", nama);
                                    userMap.put("photo", "");
                                    ref.setValue(mAuth.getUid());
                                    ref.child(mAuth.getUid()).setValue(userMap);
                                }
                                Toast.makeText(RegisterActivity.this, "Authentication succeed.", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                RegisterActivity.this.finish();
                            } else {
                                Toast.makeText(RegisterActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == btRegister.getId()) {
            createUser();
        } else if (v.getId() == btLogin.getId()) {
            Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            RegisterActivity.this.finish();
        }
    }
}